#Numerologia Pitagórica
Calcula os números da Numerologia Pitagórica baseada no nome (_original de nascimento_) e data de nascimento.

## FontEnd
Este aplicativo roda totalmente no cliente e utiliza o [**Angular.js**](https://angularjs.org/).

## Gestão de Pacotes
Utiliza [**bower**](http://bower.io/) para JavaScrip do frontend.

## Tratamento dos números
Utiliza os pacotes [**unorm**](https://github.com/walling/unorm) e [**lodash**](https://lodash.com/).

## Compilação para produção
A compilação para produção é feita através do [**Gulp**](http://gulpjs.com).

### Instalação do Gulp

	npm install

### Execução para compilação na pasta _build_

	gulp