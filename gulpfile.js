var
  gulp = require('gulp'),
  bundle = require('gulp-bundle-assets'),
  del = require('del');

gulp.task('default', function() {
  oldDelete();
  bundleCSS();
  bundleJS();
  mapsDelete();
});

gulp.task('js', function() {
  bundleJS();
});

gulp.task('delete-maps', function() {
  mapsDelete();
});

function bundleCSS() {
  return gulp.src('./bundle-configs/bundle.css.js')
    .pipe(bundle())
    .pipe(gulp.dest('./build/css'));
}

function bundleJS() {
  return gulp.src('./bundle-configs/bundle.js.js')
    .pipe(bundle())
    .pipe(gulp.dest('./build/js'));
}

function oldDelete(){
  del([
    './build/css/**',
    './build/js/**'
  ]);
}

function mapsDelete(){
  del([
    './build/css/maps/**',
    './build/js/maps/**'
  ]);
}
