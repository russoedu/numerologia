// bundle.config.js
module.exports = {
  bundle: {
    main: {
      styles: [
        './project/css/superhero.css',
        './project/css/app.css',
        './bower_components/add-to-homescreen/style/addtohomescreen.css'
      ],
      options: {
        minCSS: true,
        uglify: true,
        rev: 'production'
      }
    }
  }
};
