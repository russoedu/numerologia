// bundle.config.js
module.exports = {
  bundle: {
    main: {
      scripts: [
        './project/js/app.js',
        './project/js/controllers.js',
        './project/js/services/calcService.js',
        './project/js/services/nameCalc.js',
        './project/js/services/birthCalc.js',
        './project/js/services/nameNormalizer.js',
        './project/js/services/summer.js'
      ],
      options: {
        uglify: false,
        rev: 'production'
      }
    },
    vendor: {
      scripts: [
        './bower_components/angular/angular.min.js',
        './bower_components/angular-ui-utils/mask.min.js',
        './bower_components/ng-lodash/build/ng-lodash.min.js',
        './bower_components/add-to-homescreen/src/addtohomescreen.min.js'
      ],
      options: {
        uglify: true,
        rev: 'production'
      }
    }
  }
};
